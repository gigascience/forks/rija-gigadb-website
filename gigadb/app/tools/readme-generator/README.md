# README GENERATOR TOOL

## Preparation

In the `gigadb-website` repo root directory, spin up the GigaDB application
since we need access to the `database` container for the `gigadb` and 
`gigadb_testdata` databases.
```
$ pwd
/path/to/gigadb-website
$ ./up.sh
```

Now change directory to the `readme-generator` folder
```
$ cd gigadb/app/tools/readme-generator
```

Create a `.env` file:
```
$ cp config-sources/env.example .env
```
> Ensure you have provided values for `GITLAB_PRIVATE_TOKEN` and `REPO_NAME`
> variables.

Generate config files:
```
# Generate configuration using variables in .env, GitLab, then exit
$ docker-compose run --rm configure
```
> db.php and test_db.php should be present in the `config` directory. There 
> should be a runtime/curators directory too.

Install Composer dependencies:
```
$  docker-compose run --rm tool composer install 
```

The copying of readme files created by this tool into Wasabi requires Rclone
to be installed on your `dev` machine. This can be done using as follows:
```
# Using Homebrew
$ brew install rclone
# Or using Macports
$ sudo port install rclone
```

> There is an analogous step in the Ansible playbook for the bastion server 
> for installing Rclone on staging and live environments.
> ansible-galaxy install -r ../../../infrastructure/requirements.yml

The create readme tool uses the rclone configuration file from the wasabi
migration tool. Change directory to the `gigadb-website/gigadb/app/tools/wasabi-migration`
directory and make a copy of the `env.example` file called `.env`:
```
$ cd gigadb/app/tools/wasabi-migration
$ cp env.example .env
```

In the new `.env` file, uncomment and provide a value for the
`GITLAB_PRIVATE_TOKEN` variable.

Also provide the name of GitLab project fork in the `REPO_NAME` variable.

You should then be able to create the configuration file for rclone by
executing:
```
$ docker-compose run --rm config
```

Check if the configuration process has worked by looking for the
`config/rclone.conf` file.

## How to test

Ensure you have `bats` installed (e.g: on macOS, you could do `brew install bats-core`
or `port install bats-core`). Then run:
```
# Ensure you are in gigadb/app/tools/readme-generator directory
$ bats tests
 ✓ create readme file
 ✓ check does not create readme with invalid doi and exits
 ✓ create one readme file using batch mode
 ✓ create two readme files using batch mode

4 tests, 0 failures
```


## Using readme generator tool in dev environment

The readme information for a dataset can be viewed on standard output using its
DOI:
```
$ docker-compose run --rm tool /app/yii readme/create --doi 100142 --outdir /app/readmeFiles --bucketPath wasabi:gigadb-datasets/dev/pub/10.5524
```

Saving the readme information into a file requires a file path, specified using
`--outdir` parameter. Since `/app` has been mounted to the `readme-generator`
directory in `docker-compose.yml`, you should find a `readme_100142.txt` created
in the `readmeFiles` directory after running the above command.

The `--bucketPath` variable is essential for executing the readme tool as a 
command line tool, it is needed for constructing the location path in the `File`
table as below:

| location                                                                                                       |
|----------------------------------------------------------------------------------------------------------------|
| https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/dev/pub/10.5524/100001_101000/100142/readme_100142.txt |

Once the tool has been executed successfully, an entry in the `file` table will 
be updated/created with the updated name, location and file size, and an entry
in the `file_attributes` will be created with attribute_id `605` and the md5
value.

Information for the readme is retrieved from the `database` container that was
spun up using the `up.sh` command above. The tool is able to connect to this
container by connecting to the Docker `db-tier` network.

## Using readme generator tool via shell wrapper script in dev environment

There is a shell script which can be used to call the readme tool:
```
$ ./createReadme.sh --doi 100142
```

The `--bucketPath` variable here is not necessary, as it will be supplied to the
tool inside the script.

The `--outdir` variable is not required because the readme file will be created
in your current working directory from where the createReadme.sh script is being
called.

You should see a `readme_100142.txt` file created in your current directory.
There will also be a new log file created in the log directory which is named:
`readme.log`.

An error message is also displayed if a DOI is provided for a dataset that does 
not exist:
```
$ ./createReadme.sh --doi 1
Dataset 1 not found
```

### Wasabi upload of readme files

The readme tool is also able to copy readme files it creates into Wasabi:
```
# Execute wasabi upload in dry run mode
$ ./createReadme.sh --doi 100142 --wasabi
```

The log/readme.log file will confirm dry run mode Wasabi upload:
```
2024/10/03 11:25:40 INFO  : Created readme file for DOI 100142 in /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100142.txt
2024/10/03 11:25:41 NOTICE: readme_100142.txt: Skipped update modification time as --dry-run is set (size 1.949Ki)
2024/10/03 11:25:41 NOTICE: 
Transferred:   	          0 B / 0 B, -, 0 B/s, ETA -
Elapsed time:         0.4s

2024/10/03 11:25:41 INFO  : Executed: rclone copy --s3-no-check-bucket /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100142.txt wasabi:gigadb-datasets/dev/pub/10.5524/100001_101000/100142/ --config /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/../wasabi-migration/config/rclone.conf --dry-run --log-file /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log --log-level INFO --stats-log-level DEBUG >> /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log
2024/10/03 11:25:41 INFO  : Successfully copied file to Wasabi for DOI: 100142
```

Using the `--apply` flag will switch off dry run mode and copy the readme file
into the gigadb-datasets/dev bucket:
```
# Confirm actual wasabi upload of files using apply flag
$ ./createReadme.sh --doi 100142 --wasabi --apply
```

The latest log messages in log/readme.log should confirm Wasabi upload:
```
2024/10/03 11:26:40 INFO  : Created readme file for DOI 100142 in /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100142.txt
2024/10/03 11:26:41 INFO  : readme_100142.txt: Updated modification time in destination
2024/10/03 11:26:41 INFO  : Executed: rclone copy --s3-no-check-bucket /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100142.txt wasabi:gigadb-datasets/dev/pub/10.5524/100001_101000/100142/ --config /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/../wasabi-migration/config/rclone.conf --log-file /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log --log-level INFO --stats-log-level DEBUG >> /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log
2024/10/03 11:26:41 INFO  : Successfully copied file to Wasabi for DOI: 100142
```

### Batch processing of readme files

The createReadme.sh script has a batch processing mode which can be accessed
using the `--batch` flag:
```
# Create 2 readme files
$ ./createReadme.sh --doi 100005 --batch 2
```

The `--batch` flag takes a value equal to the number of readme files you would
like to successfully create. The above command will create a total of 2 readme
files which its log file will confirm:

```
2024/10/03 11:34:26 WARN  : No dataset for DOI 100005
2024/10/03 11:34:27 INFO  : Created readme file for DOI 100006 in /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100006.txt
2024/10/03 11:34:27 NOTICE: readme_100006.txt: Skipped copy as --dry-run is set (size 3.180Ki)
2024/10/03 11:34:27 NOTICE: 
Transferred:   	    3.180 KiB / 3.180 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         0.3s

2024/10/03 11:34:27 INFO  : Executed: rclone copy --s3-no-check-bucket /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100006.txt wasabi:gigadb-datasets/dev/pub/10.5524/100001_101000/100006/ --config /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/../wasabi-migration/config/rclone.conf --dry-run --log-file /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log --log-level INFO --stats-log-level DEBUG >> /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log
2024/10/03 11:34:27 INFO  : Successfully copied file to Wasabi for DOI: 100006
2024/10/03 11:34:29 WARN  : No dataset for DOI 100007
2024/10/03 11:34:30 WARN  : No dataset for DOI 100008
2024/10/03 11:34:31 WARN  : No dataset for DOI 100009
2024/10/03 11:34:31 WARN  : No dataset for DOI 100010
2024/10/03 11:34:33 WARN  : No dataset for DOI 100011
2024/10/03 11:34:33 WARN  : No dataset for DOI 100012
2024/10/03 11:34:34 WARN  : No dataset for DOI 100013
2024/10/03 11:34:35 WARN  : No dataset for DOI 100014
2024/10/03 11:34:36 WARN  : No dataset for DOI 100015
2024/10/03 11:34:36 WARN  : No dataset for DOI 100016
2024/10/03 11:34:37 WARN  : No dataset for DOI 100017
2024/10/03 11:34:43 WARN  : No dataset for DOI 100018
2024/10/03 11:34:47 WARN  : No dataset for DOI 100019
2024/10/03 11:34:48 INFO  : Created readme file for DOI 100020 in /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100020.txt
2024/10/03 11:34:49 NOTICE: readme_100020.txt: Skipped update modification time as --dry-run is set (size 2.592Ki)
2024/10/03 11:34:49 NOTICE: 
Transferred:   	          0 B / 0 B, -, 0 B/s, ETA -
Elapsed time:         0.3s

2024/10/03 11:34:49 INFO  : Executed: rclone copy --s3-no-check-bucket /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/readme_100020.txt wasabi:gigadb-datasets/dev/pub/10.5524/100001_101000/100020/ --config /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/../wasabi-migration/config/rclone.conf --dry-run --log-file /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log --log-level INFO --stats-log-level DEBUG >> /Volumes/PLEXTOR/PhpstormProjects/pli888/gigadb-website/gigadb/app/tools/readme-generator/log/readme.log
2024/10/03 11:34:49 INFO  : Successfully copied file to Wasabi for DOI: 100020
```

## Tests

### Unit test

The unit test checks custom-written `getAuthors` function in Dataset model class
which returns the authors of a dataset based on many-to-many mapping between
dataset and author tables via a junction `dataset_authors` table:
```
$ docker-compose run --rm tool ./vendor/bin/codecept run tests/unit
```

There's also a unit test to check the ReadmeGenerator component class:
```
$ docker-compose run --rm tool ./vendor/bin/codecept run --debug tests/unit/components/ReadmeGeneratorTest.php
```

### Functional test

There is a functional test which checks the `actionCreate()` function in
`ReadmeController`.
```
$ docker-compose run --rm tool ./vendor/bin/codecept run tests/functional

# Run single test
$ docker-compose run --rm tool ./vendor/bin/codecept run tests/functional/ReadmeCest.php
```

## Using readme generator tool on Bastion server

Before running the bastion playbook, execute the following command to install
the ansible-rclone role for Ansible in your development environment:
```
$ ansible-galaxy install -r ../../../infrastructure/requirements.yml
```

Running the bastion playbook will copy `createReadme.sh` script to the server:
```
$ env OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES ansible-playbook -i ../../inventories bastion_playbook.yml
```

If changes are made to `createReadme.sh`, this part of the bastion playbook will
need to be executed again to copy the updated shell script to the server:
```
$ env OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES ansible-playbook -i ../../inventories bastion_playbook.yml --tags "readme_tool"
```

Instantiate and log into bastion server:
```
# Get public IP address for bastion server
$ terraform output
ec2_bastion_private_ip = "10.88.8.888"
ec2_bastion_public_ip = "88.888.888.888"

# Log into bastion server
$ ssh -i ~/.ssh/your-private-key.pem ec2-user@88.888.888.888
```

Before executing the `createReadme` tool, get the existing values of the `file` table and `file_attributes` table:
```
# check the tables
[ec2-user@ip-10-99-0-207 ~]$ psql -h $rds_instance_address -U gigadb -c 'select id, name, location, size from file where dataset_id = 200'
Password for user gigadb: 
  id   |                      name                       |                                                                   location                                                                    |   size    
-------+-------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+-----------
 87517 | Diagram-ALL-FIELDS-Check-annotation.jpg         | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/Diagram-ALL-FIELDS-Check-annotation.jpg         |     55547
 87540 | SRAmetadb.zip                                   | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/SRAmetadb.zip                                   | 383892184
 87542 | Diagram-SRA-Study-Experiment-Joined-probing.jpg | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/Diagram-SRA-Study-Experiment-Joined-probing.jpg |     81717
 87516 | readme.txt                                      | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/readme.txt                                      |      2351
(4 rows)
[ec2-user@ip-10-99-0-207 ~]$ psql -h $rds_instance_address -U gigadb -c 'select * from file_attributes where file_id = 87516'
Password for user gigadb: 
  id   | file_id | attribute_id |               value                | unit_id 
-------+---------+--------------+------------------------------------+---------
 17051 |   87516 |          605 | c60c299fdf375b28cd444e70f43fa398   | 
(1 row)
```

Using docker command to access tool:
```
$ docker run --rm -v /home/ec2-user/readmeFiles:/app/readmeFiles registry.gitlab.com/$GITLAB_PROJECT/production_tool:$GIGADB_ENV /app/yii readme/create --doi 100142 --outdir /app/readmeFiles --bucketPath wasabi:gigadb-datasets/$GIGADB_ENV/pub/10.5524
```

Check the tables `file` and `file_attribbutes` that `name`, `location`, `size` and `value` have been updated.
```
[ec2-user@ip-10-99-0-207 ~]$ psql -h rds-server-staging-ken.cjizsjwbxkxv.ap-northeast-2.rds.amazonaws.com -U gigadb -c 'select id, name, location, size from file where dataset_id = 200'
Password for user gigadb: 
  id   |                      name                       |                                                                   location                                                                    |   size    
-------+-------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+-----------
 87516 | readme_100142.txt                               | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/staging/pub/10.5524/100001_101000/100142/readme_100142.txt                            |      1672
 87540 | SRAmetadb.zip                                   | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/SRAmetadb.zip                                   | 383892184
 87542 | Diagram-SRA-Study-Experiment-Joined-probing.jpg | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/Diagram-SRA-Study-Experiment-Joined-probing.jpg |     81717
 87517 | Diagram-ALL-FIELDS-Check-annotation.jpg         | https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100142/Diagram-ALL-FIELDS-Check-annotation.jpg         |     55547
(4 rows)
[ec2-user@ip-10-99-0-207 ~]$ psql -h rds-server-staging-ken.cjizsjwbxkxv.ap-northeast-2.rds.amazonaws.com -U gigadb -c 'select * from file_attributes where file_id = 87516'
Password for user gigadb: 
  id   | file_id | attribute_id |              value               | unit_id 
-------+---------+--------------+----------------------------------+---------
 17051 |   87516 |          605 | 0fe8d1309283ffac54d782fd44434f9e | 
(1 row)

```

Check a readme file has been created:
```
$ head readmeFiles/readme_100142.txt 
[DOI] 10.5524/100142

[Title] Supporting scripts and data for "Investigation into the annotation of
protocol sequencing steps in the Sequence Read Archive".
```

Check the rclone cmd is working:
```
$ rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readmeFiles/readme_100925.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100142/ --config /home/ec2-user/.config/rclone/rclone.conf --dry-run
2025/02/27 03:20:01 NOTICE: readme_100142.txt: Skipped copy as --dry-run is set (size 9.729Ki)
2025/02/27 03:20:01 NOTICE: 
Transferred:        9.729 KiB / 9.729 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         1.3s
```

Use shell wrapper script to run readme tool and copy readme file to Wasabi:
```
$ /usr/local/bin/createReadme --doi 100142
```

This time, you can check the log of this create readme file command:
```
$ more var/log/gigadb/readme_ec2-user.log
2024/12/13 06:44:13 INFO  : Created readme file for DOI 100142 in /home/ec2-user/readme_100142.txt
```

The createReadme.sh script can also be used to copy the newly created readme 
file into the Wasabi gigadb-datasets bucket. To test this in dry-run mode,
execute:
```
$ /usr/local/bin/createReadme --doi 100142 --wasabi
```

And then check the rclone log:
```
[ec2-user@ip-10-99-0-207 ~]$ more /var/log/gigadb/readme_ec2-user.log
22025/02/27 03:23:05 INFO  : Created readme file for DOI 100142 in /home/ec2-user/readme_100142.txt
2025/02/27 03:23:08 NOTICE: readme_100142.txt: Skipped copy as --dry-run is set (size 2.022Ki)
2025/02/27 03:23:08 NOTICE: 
Transferred:        2.022 KiB / 2.022 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         2.2s

2025/02/27 03:23:08 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readme_100142.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100142/ --config /home/ec2-user/.conf
ig/rclone/rclone.conf --dry-run --log-file /var/log/gigadb/readme_ec2-user.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:23:08 INFO  : Successfully copied file to Wasabi for DOI: 100142
```

If you look at the latest log file in the logs directory, you will see the
destination path that the readme file will be copied to which will be in the 
staging directory. You can deactivate dry-run mode using the --apply flag:
```
$ /usr/local/bin/createReadme --doi 100142 --wasabi --apply
```

And the rclone log will be:
```
[ec2-user@ip-10-99-0-207 ~]$ more /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:24:19 INFO  : Created readme file for DOI 100142 in /home/ec2-user/readme_100142.txt
2025/02/27 03:24:21 INFO  : readme_100142.txt: Copied (replaced existing)
2025/02/27 03:24:21 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readme_100142.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100142/ --config /home/ec2-user/.conf
ig/rclone/rclone.conf --log-file /var/log/gigadb/readme_ec2-user.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:24:21 INFO  : Successfully copied file to Wasabi for DOI: 100142
```

You can confirm that the presence of the new readme file in the 100142 directory
using the Wasabi web console by checking the gigadb-datasets/staging bucket.

There is a batch mode for the script which can be used by providing the 
`--batch` flag followed by a number to denote the number of datasets to be
processed. For example, to process DOIs 100141, 100142, 100143:
```
$ /usr/local/bin/createReadme --doi 100141 --wasabi --batch 3
```

You will be able to see in the latest log file in the logs directory that 3
readme files have been created and copied into Wasabi in dry-run mode.

```
[ec2-user@ip-10-99-0-207 ~]$ more /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:25:47 INFO  : Created readme file for DOI 100141 in /home/ec2-user/readme_100141.txt
2025/02/27 03:25:49 NOTICE: readme_100141.txt: Skipped copy as --dry-run is set (size 5.443Ki)
2025/02/27 03:25:49 NOTICE: 
Transferred:        5.443 KiB / 5.443 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         1.3s

2025/02/27 03:25:49 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readme_100141.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100141/ --config /home/ec2-user/.conf
ig/rclone/rclone.conf --dry-run --log-file /var/log/gigadb/readme_ec2-user.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:25:49 INFO  : Successfully copied file to Wasabi for DOI: 100141
2025/02/27 03:25:52 INFO  : Created readme file for DOI 100142 in /home/ec2-user/readme_100142.txt
2025/02/27 03:25:54 NOTICE: readme_100142.txt: Skipped copy as --dry-run is set (size 2.039Ki)
2025/02/27 03:25:54 NOTICE: 
Transferred:        2.039 KiB / 2.039 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         1.3s

2025/02/27 03:25:54 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readme_100142.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100142/ --config /home/ec2-user/.conf
ig/rclone/rclone.conf --dry-run --log-file /var/log/gigadb/readme_ec2-user.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:25:54 INFO  : Successfully copied file to Wasabi for DOI: 100142
2025/02/27 03:25:58 INFO  : Created readme file for DOI 100143 in /home/ec2-user/readme_100143.txt
2025/02/27 03:26:00 NOTICE: readme_100143.txt: Skipped copy as --dry-run is set (size 12.636Ki)
2025/02/27 03:26:00 NOTICE: 
Transferred:       12.636 KiB / 12.636 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         1.3s

2025/02/27 03:26:00 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/ec2-user/readme_100143.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100143/ --config /home/ec2-user/.conf
ig/rclone/rclone.conf --dry-run --log-file /var/log/gigadb/readme_ec2-user.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_ec2-user.log
2025/02/27 03:26:00 INFO  : Successfully copied file to Wasabi for DOI: 100143
[ec2-user@ip-10-99-0-171 ~]$
```

To copy the readme file to the live data directory, use the `--use-live-data`
and `--apply` flags:
```
$ /usr/local/bin/createReadme --doi 100142 --wasabi --use-live-data --apply
```

Now check the directory for dataset 100142 in relevant location in
gigadb-datasets/live bucket in Wasabi.

## Using readme generator tool on Bastion server as a user

Execute the following command to create a user in the bastion server if needed:

```
$ cd ops/infrastructure/envs/staging
% ansible-playbook -i ../../inventories users_playbook.yml -e "newuser=lily" -e "credentials_csv_path=/path/to/user/wasabi/credential/file" --extra-vars="gigadb_env=staging"
```

Log into the bastion server as the new user:
```
$ ssh -i output/privkeys-88.888.888.888/lily lily@88.888.888.888

# check the rclone cmd is working
[lily@ip-10-99-0-171 ~]$ rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/lily/readme_100925.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100925/ --config /home/lily/.config/rclone/rclone.conf --dry-run 
2025/02/27 03:12:04 NOTICE: readme_100925.txt: Skipped copy as --dry-run is set (size 9.731Ki)
2025/02/27 03:12:04 NOTICE: 
Transferred:        9.731 KiB / 9.731 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         2.2s
[lily@ip-10-99-0-171 ~]$ /usr/local/bin/createReadme 
Usage: /usr/local/bin/createReadme --doi <DOI>

Required:
--doi            DOI to process

Available Options:
--batch          Number of DOI to process
--wasabi         (Default) Copy readme file to Wasabi bucket
--apply          Escape dry run mode
--use-live-data  Copy data to production live bucket
[lily@ip-10-99-0-171 ~]$ ls /var/log/gigadb/
readme_ec2-user.log
[lily@ip-10-99-0-171 ~]$ /usr/local/bin/createReadme --doi 100925
[lily@ip-10-99-0-171 ~]$ ls /var/log/gigadb/
readme_ec2-user.log  readme_lily.log
[lily@ip-10-99-0-171 ~]$ ls /var/log/gigadb/
readme_ec2-user.log  readme_lily.log
[lily@ip-10-99-0-171 ~]$ more /var/log/gigadb/readme_lily.log
2025/02/27 03:37:42 INFO  : Created readme file for DOI 100925 in /home/lily/readme_100925.txt
2025/02/27 03:37:43 NOTICE: readme_100925.txt: Skipped copy as --dry-run is set (size 9.731Ki)
2025/02/27 03:37:43 NOTICE: 
Transferred:        9.731 KiB / 9.731 KiB, 100%, 0 B/s, ETA -
Transferred:            1 / 1, 100%
Elapsed time:         1.3s

2025/02/27 03:37:43 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/lily/readme_100925.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100925/ --config /home/lily/.config/rclone/rclone.conf --dry-run --log-file /var/log/gigadb/readme_lily.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_lily.log
2025/02/27 03:37:43 INFO  : Successfully copied file to Wasabi for DOI: 100925
[lily@ip-10-99-0-171 ~]$ 
# execute the script with --apply flag
[lily@ip-10-99-0-171 ~]$ /usr/local/bin/createReadme --doi 100925 --apply
[lily@ip-10-99-0-171 ~]$ more /var/log/gigadb/readme_lily.log
2025/02/27 03:39:56 INFO  : Created readme file for DOI 100925 in /home/lily/readme_100925.txt
2025/02/27 03:39:57 INFO  : readme_100925.txt: Copied (replaced existing)
2025/02/27 03:39:57 INFO  : Executed: rclone copy --s3-no-check-bucket --s3-profile wasabi-transfer /home/lily/readme_100925.txt wasabi:gigadb-datasets/staging/pub/10.5524/100001_101000/100925/ --config /home/lily/.config/rclone/rclone.conf --log-file /var/log/gigadb/readme_lily.log --log-level INFO --stats-log-level DEBUG >> /var/log/gigadb/readme_lily.log
2025/02/27 03:39:57 INFO  : Successfully copied file to Wasabi for DOI: 100925
```

