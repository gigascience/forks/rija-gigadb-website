export function getContainerDimensions(container) {
  return [container.innerWidth(), container.innerHeight()];
}
